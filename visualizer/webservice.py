"""
Import flask functions & utility functions to access data from utility.py
"""

from flask import Flask, jsonify, abort, render_template, request
from utility import load_array, read_json, nn_content, get_view, thread_content


app = Flask(__name__)


# current view represented as list. 0 = left child, 1 = right child (current_view = [0, 0, 1] => current_view[0][0][1] => root-node, left, left, right)
current_view = []

@app.route('/visualizer')
@app.route('/visualizer/')
def page_home():
	"""
		When requesting '/visualizer' or '/visualizer/' on localhost, you'll get the home.html template
	"""
	return render_template('home.html')


@app.route('/visualizer/settings')
@app.route('/visualizer/settings/')
def page_settings():
	"""
		When requesting '/visualizer/settings' or '/visualizer/settings/' on localhost, you'll get the settings.html template
	"""
	return render_template('settings.html')


@app.route('/visualizer/points', methods=['GET'])
def get_points():
	"""
		On request via GET this returns the complete list of points

		:returns data:
			JSON list of all point coordinates
	"""
	data = get_view([]).tolist()
	return jsonify({'points': data})


@app.route('/visualizer/points/<int:id>', methods=['GET'])
def change_view(id):
	"""
		On request via GET this changes the current view and returns corresponding data coordinates
		0: left child node
		1: right child node
		2: previous view

		:returns data:
			JSON object containing the coordinates of the requested node
	"""

	# ID = 2 => load previous view
	if id == 2 and not len(current_view) == 0:
		current_view.pop()

	# if current_view is empty load get_view([]), this will return root node view
	#elif len(current_view) == 0:
	#	data_coordinates = "LOAD NPY ARRAY FOR ROOT NODE"


#TODO Abbruch-Bedingung!
	# ID = 0 => load view of left child node
	if id == 0:
			current_view.append(0)

	# ID = 1 => load view of right child node
	if id == 1:
			current_view.append(1)

	data = get_view(current_view).tolist()
	return jsonify({'points': data})


@app.route('/visualizer/threads/<int:id>', methods=['GET'])
def get_thread_dep(id):
	"""
		On request via GET this returns the post content of the id-th datapoint

		:returns data:
			JSON object containing the information of the id-th datapoint
	"""
	if (len(list_threads) == 0) or (id >= len(list_threads)):
		abort(404)
	return_object = list_threads[id]
	return jsonify({'thread': return_object})


@app.route('/visualizer/thread/', methods=['POST'])
def get_thread():
	"""
		On request via POST compute information and return post information
	"""

	data_json = request.get_json()
	point = data_json["point"]
	
	data_response = thread_content(point, current_view)

	return jsonify(data_response)


@app.route('/visualizer/nearest-neighbors/', methods=['POST'])
def get_nn():
	"""
		on request via POST compute the three nearest neighbors and retrieve their content
	"""
	data_json = request.get_json()
	point = data_json["point"]
	nearest_neighbors_content = nn_content(point[:2], 3, current_view)

	return jsonify(nearest_neighbors_content)



if __name__ == '__main__':
    app.run(debug=True)