from utility import *
import numpy as np

# config
use_pca_data = True
n_tsne_runs = 10
root_directory = "data-9be223ea"
threads = "../data/jesusThreadsEmbeddings_comma.csv"
threads_json = "../data/posts.json_paragraphEmbedding.json"
tree = load_array('files/hierarchy-79dd916c.npy')
#tree = load_array(root_directory + 'hierarchy.npy')

def init():
	# load threads
	data = read_csv(threads)

	# run pca if needed
	if use_pca_data:
		data = run_pca(data)

	# compute root view with best kl-divergence from n_tsne_runs and save result
	tsne_dict = {}
	for i in range(n_tsne_runs):
		tsne = run_tsne_kl(data_pca)
		tsne_dict[tsne["kl_divergence"]] = tsne["data"]

	keys = np.array(tsne_dict.keys())
	save_array(tsne_dict[keys.min()], root_directory + '/view-root.npy')

	# compute hierarchie
	tree = run_clustering(data)
	save_array(tree, root_directory + "/hierarchy.npy")


#################################################
# run init() once to compute necessary files
# init()
#################################################