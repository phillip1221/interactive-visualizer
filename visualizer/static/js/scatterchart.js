/*
*
* Interactive Visualizer Scatterchart
*
*/


// helper variables
var width = 1500,
	height = 750;
var link = "http://www.jesus.de/forum/ansicht/thread.html?tx_scmforum_pi1[post_uid]=";
var alertTemplate = $("#alert-template").html();
var data_json = [];

$(document).ready(
	/*
	*	on document ready
	*/
	function() {
		/*
		*	load initial visualization from root node and draw plot
		*/
		$.getJSON("/visualizer/points", function(result){
			data_json = result.points;
			//console.log(data_json)
			draw_plot(data_json)
		});

		// add button events
		$("#subcluster-red").on("click", 0, loadSubcluster);
		$("#subcluster-blue").on("click", 1, loadSubcluster);
		$("#cluster-return").on("click", 2, loadSubcluster);
	}
);





function showInformation(point) {
	/*
	*	get point as [x, y, label] and display information in info section
	*/

	// update category information
	$("#point").empty();
	$("#point").append("<li class=\"list-group-item active\" id=\"point-info\">currently selected</li>");
	$("#point").append("<li class=\"list-group-item justify-content-between\" id=\"point-x\"></li>\n<li class=\"list-group-item justify-content-between\" id=\"point-y\"></li>\n<li class=\"list-group-item justify-content-between\" id=\"point-label\"></li>\n");
	$("#point-x").append(point[0] + "<span class=\"badge badge-default badge-pill\">x-coordinate </span>");
	$("#point-y").append(point[1] + "<span class=\"badge badge-default badge-pill\">y-coordinate</span>");
	$("#point-label").append(point[2] + "<span class=\"badge badge-default badge-pill\">clusterlabel</span>");
/*		
	Alternative Darstellung
	$("#point-x").append("x-coordinate <span class=\"badge badge-default badge-pill\">" + point[0] + "</span>");
	$("#point-y").append("y-coordinate <span class=\"badge badge-default badge-pill\">" + point[1] + "</span>");
	$("#point-label").append("clusterlabel <span class=\"badge badge-default badge-pill\">" + point[2] + "</span>");
*/

	// hide post and neighbor sections with information on previously selected point
	$("#post").css("display", "none");
	$("#neighbors").css("display", "none");

	// update button onlick events
	// use parameter to avoid firing the click event due to ()
	$("#btn-post").off().on("click", {"point": point}, showPost);
	$("#btn-nn").off().on("click", {"point": point}, showNeighbors);

	// remove disabled attribute on buttons (only initially needed)
	$("#btn-post").removeAttr("disabled");
	$("#btn-nn").removeAttr("disabled");
}

function showPost(event) {
	/*
	*	send currently selected point as json object via POST and get the corresponding post as response
	*/

	// access data object from click event and generate json from selected 'point'
	data_json = JSON.stringify(event.data);

	$.ajax ({
		url: "/visualizer/thread/",
		type: "POST",
		data: data_json,
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function(data_response) {
			// display infobox
			// empty current content and update information according to response
			$("#post").css("display", "block");
			$("#post-header").empty().append("<b>Author</b> " + data_response.author);
			$("#post-content").empty().append("<a href=\"" + link + data_response.thread + "\" target=\"_blank\" class=\"btn btn-block btn-info\">show corresponding thread</a>\n<hr />" + data_response.html);
			$("#post-footer").empty().append("<b>Date</b> " + data_response.published);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			// display feedback section
			// empty current content and update text from response if error occured
			$("#feedback").css("display", "block");
			$("#feedback div").append(alertTemplate);
			$("#alert-content").empty().append(jqXHR.responseText);
			$("body, html").animate({scrollTop: $("#feedback").offset().top - 75}, 1000);
		}
	});
}

function showNeighbors(event) {
	/*
	*	send currently selected point as json object via POST and get the content of the nearest neighbors as response
	*/

	// access data object from click event and generate json from selected'point'
	data_json = JSON.stringify(event.data);

	$.ajax ({
		url: "/visualizer/nearest-neighbors/",
		type: "POST",
		data: data_json,
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function(data_response) {
			// display neighbors accordion
			// empty current content and update information with three nearest neighbors content
			$("#neighbors").css("display", "block");
			$("#neighbor-1-header a").empty().append(data_response[0].author + ", "+ data_response[0].published);
			$("#neighbor-1-content").empty().append("<a href=\"" + link + data_response[0].thread + "\" target=\"_blank\" class=\"btn btn-block btn-info\">show corresponding thread</a>\n<hr />" + data_response[0].html);
			$("#neighbor-2-header a").empty().append(data_response[1].author + ", "+ data_response[1].published);
			$("#neighbor-2-content").empty().append("<a href=\"" + link + data_response[1].thread + "\" target=\"_blank\" class=\"btn btn-block btn-info\">show corresponding thread</a>\n<hr />" + data_response[1].html);
			$("#neighbor-3-header a").empty().append(data_response[2].author + ", "+ data_response[2].published);
			$("#neighbor-3-content").empty().append("<a href=\"" + link + data_response[2].thread + "\" target=\"_blank\" class=\"btn btn-block btn-info\">show corresponding hread</a>\n<hr />" + data_response[2].html);

			// reset accordion-boxes to be closed
			$("#neighbor-1-content").removeClass("show");
			$("#neighbor-2-content").removeClass("show");
			$("#neighbor-3-content").removeClass("show");
		},
		error: function(jqXHR, textStatus, errorThrown) {
			// display feedback section
			// empty current content and update text from response if error occured
			$("#feedback").css("display", "block");
			$("#feedback div").append(alertTemplate);
			$("#alert-content").empty().append(jqXHR.responseText);
			$("body, html").animate({scrollTop: $("#feedback").offset().top - 75}, 1000);
		}
	});
}


function loadSubcluster(event) {
	/*
	*	clear current view and load & plot subcluster
	*/
	var subluster = event.data;

	$.ajax ({
		url: "/visualizer/points/" + subluster,
		type: "GET",
		dataType: "json",
		success: function(data_response) {
			// remove svg to clear current plot
			$("#chart-content").remove();

			// console.log(data_response);

			// draw plot from data_response
			data_json = data_response.points;
			draw_plot(data_json)
		},
		error: function(jqXHR, textStatus, errorThrown) {
			// display feedback section
			// empty current content and update text from response if error occured
			$("#feedback").css("display", "block");
			$("#feedback div").append(alertTemplate);
			$("#alert-content").empty().append(jqXHR.responseText);
			$("body, html").animate({scrollTop: $("#feedback").offset().top - 75}, 1000);
		}
	});

	// hide post and neighbor sections with information on previously selected point
	$("#post").css("display", "none");
	$("#neighbors").css("display", "none");
}


function draw_plot(data) {
	/*
	*	draw and render plot from given data
	*/
	// scale x-axis
	// compute min and max value for x-coordinates (+/- 0.1 to add space)
	var x = d3.scaleLinear()
					.domain([d3.min(data, function(d){return d[0];}) - 0.1, d3.max(data, function(d){return d[0];}) + 0.1])
					.range([0, width]);

	// scale y-axis
	// compute min and max value for y-coordinates (+/- 0.1 to add space)
	var y = d3.scaleLinear()
					.domain([d3.min(data, function(d){return d[1];}) - 0.1, d3.max(data, function(d){return d[1];}) + 0.1])
					.range([height, 0]);

	// append svg to display chart
	var chart = d3.select("#chart")
					.append("svg:svg")
					.attr("id", "chart-content")
					.attr("viewBox", "0 0 " + width + " " + height)
					.attr("preserveAspectRatio", "xMinYMin");

	var main = chart.append("g")
					.attr("transform", "translate(0, 0)")
					.attr("width", width)
					.attr("height", height);

	/* ! category10 makes irregular use of color labels, sometimes 0 is blue and sometimes it is red ! -> switched to manual color function */
	// use category10 color scale (10 predefined and distinct css colors)
	// var color = d3.scaleOrdinal(d3.schemeCategory10);
	var color = function(label) {
		if(label == 0) return "#ff7f00"; // red/orange
		if(label == 1) return "#1776b6"; // blue
	}

	// draw circles from input data
	var svg = main.append("svg:g");
	svg.selectAll("scatter-dots")
			.data(data)
			.enter()
			.append("svg:circle")
				.attr("cx", function(d) {return x(d[0]);})			// scaled x-coordinate
				.attr("cy", function(d) {return y(d[1]);})			// scaled y-coordinate
				.attr("r", 3)										// set size/radius
				.style("fill", function(d) {return color(d[2]);})	// set color using label information
				.on("click", showInformation);						// add onclick event
}