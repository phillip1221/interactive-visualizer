# Initialization Variables
from init import root_directory, threads, threads_json, tree

def read_csv(filename):
	"""
	Read CSV files

	Parameters
	----------
	filename : string
		path to file to be loaded

	Returns
	-------
	array : ndarray
		array of values

	Notes
	-----
	configurated to read pure csv files without header delimeted by commata
	"""

	from pandas import read_csv

	dataframe = read_csv(filename, header=None)
	return dataframe.values


def read_json(filename):
	"""
	Read JSON files

	Parameters
	----------
	filename : string
		path to file to be loaded

	Returns
	-------
	object : json-object
		object 

	Notes
	-----
	deserialized object after json conversation table
	"""

	import json

	with open(filename) as json_data:
		d = json.load(json_data)

	return d


def save_array(data, filename):
	"""
	Save input data as numpy array

	Parameters
	----------
	data : ndarray
		input data
	filename : string
		filename including path where file is stored

	Notes
	-----
	saves input data as numpy array to given path and name
	"""

	import numpy as np

	array = np.array(data)
	np.save(filename, array)


def load_array(filename):
	"""
	Load numpy array

	Parameters
	----------
	filename : string
		path to file to be loaded including path

	Returns
	-------
	array : numpy array
		array holding the data from given file 

	Notes
	-----
	numpy array
	"""

	import numpy as np

	array = np.load(filename)
	return array


def run_clustering(data):
	"""
	compute agglomerative hierarchical clustering on input data

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties

	Returns
	-------
	cluster : object
		providing access to attributes below

	Providing Attributes
	-------
	labels_ : array [n_samples]
		cluster labels for each point
	n_leaves_ : int
		Number of leaves in the hierarchical tree.
	n_components_ : int
		The estimated number of connected components in the graph.
	children_ : array-like, shape (n_nodes-1, 2)
		The children of each non-leaf node. Values less than n_samples correspond to leaves of the tree which are the original samples. A node i greater than or equal to n_samples is a non-leaf node and has children children_[i - n_samples]. Alternatively at the i-th iteration, children[i][0] and children[i][1] are merged to form node n_samples + i

	Notes
	------
	recursively merges the pair of clusters that minimally increases a given linkage distance
	"""

	from sklearn import cluster, preprocessing

	data_scaled = preprocessing.scale(data)
	cluster = cluster.AgglomerativeClustering(affinity='cosine', linkage='complete', compute_full_tree=True).fit(data_scaled)
	return cluster


def run_pca(data):
	"""
	compute principal component analysis on input data

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties
	(n_components : int
		number of principal components to be computed, fixed to 100)

	Returns
	-------
	data_reduced_pca : object
		access to input data projected on n_components principal components

	Providing Attributes
	-------
	components_ : array, [n_components, n_features]
		Principal axes in feature space, representing the directions of maximum variance in the data. The components are sorted by explained_variance_.
	explained_variance_ : array, [n_components]
		The amount of variance explained by each of the selected components.
	explained_variance_ratio_ : array, [n_components]
		Percentage of variance explained by each of the selected components.
		If n_components is not set then all components are stored and the sum of explained variances is equal to 1.0.
	mean_ : array, [n_features]
		Per-feature empirical mean, estimated from the training set.
		Equal to X.mean(axis=1).
	n_components_ : int
		The estimated number of components. When n_components is set to 'mle' or a number between 0 and 1 (with svd_solver == 'full') this number is estimated from input data. Otherwise it equals the parameter n_components, or n_features if n_components is None.
	noise_variance_ : float
		The estimated noise covariance following the Probabilistic PCA model from Tipping and Bishop 1999. See 'Pattern Recognition and Machine Learning' by C. Bishop, 12.2.1 p. 574 or http://www.miketipping.com/papers/met-mppca.pdf. It is required to computed the estimated data covariance and score samples.

	Notes
	------
	linear dimensionality reduction on given input data returning its data projected onto the principal components
	"""

	from sklearn import preprocessing, decomposition

	data_scaled = preprocessing.scale(data)
	pca = decomposition.PCA(n_components=100)
	data_reduced_pca = pca.fit_transform(data_scaled)
	return data_reduced_pca


def run_kpca(data):
	"""
	compute principal component analysis on input data

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties
	(n_components : int
		number of principal components to be computed, fixed to 100)

	Returns
	-------
	data_reduced_pca : object
		acces to input data projected on principal components

	Providing Attributes
	-------
	lambdas_ : array, (n_components,)
		Eigenvalues of the centered kernel matrix in decreasing order. If n_components and remove_zero_eig are not set, then all values are stored.
	alphas_ : array, (n_samples, n_components)
		Eigenvectors of the centered kernel matrix. If n_components and remove_zero_eig are not set, then all components are stored.
	dual_coef_ : array, (n_samples, n_features)
		Inverse transform matrix. Set if fit_inverse_transform is True.
	X_transformed_fit_ : array, (n_samples, n_components)
		Projection of the fitted data on the kernel principal components.
	X_fit_ : (n_samples, n_features)
		The data used to fit the model. If copy_X=False, then X_fit_ is a reference. This attribute is used for the calls to transform.

	Notes
	------
	non-linear dimensionality reduction through the use of kernels on given input data returning its data projected onto the principal components
	"""

	from sklearn import decomposition

	kpca = decomposition.KernelPCA(n_components=2, kernel='rbf', gamma=15)
	data_reduced_kpca = kpca.fit_transform(data)
	return data_reduced_kpca


def run_tsne(data):
	"""
	compute t-distributed stochastic neighbor embedding

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties

	Returns
	-------
	data_reduced_tsne : ndarray
		data mapped into two/three dimensions

	Providing Attributes
	-------
	embedding_ : array-like, shape (n_samples, n_components)
		Stores the embedding vectors.
	kl_divergence_ : float
		Kullback-Leibler divergence after optimization.

	Notes
	------
	using cosine as metric since data is computed to represent cosine similarity
	"""

	from sklearn import manifold
	
	#tsne = manifold.TSNE(n_components=2, perplexity=30.0, early_exaggeration=4.0, learning_rate=1000.0, n_iter=1000, n_iter_without_progress=30, min_grad_norm=1e-07, metric='euclidean', init='random', verbose=0, random_state=None, method='barnes_hut', angle=0.5)
	tsne = manifold.TSNE(metric='cosine')
	data_reduced_tsne = tsne.fit_transform(data)
	return data_reduced_tsne


def run_tsne(data, data_initial):
	"""
	compute t-distributed stochastic neighbor embedding

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties
	data_initial : ndarray
		numpy array containing two dimensional starting points for optimization

	Returns
	-------
	data_reduced_tsne : ndarray
		data mapped into two/three dimensions

	Providing Attributes
	-------
	embedding_ : array-like, shape (n_samples, n_components)
		Stores the embedding vectors.
	kl_divergence_ : float
		Kullback-Leibler divergence after optimization.

	Notes
	------
	using cosine as metric since data is computed to represent cosine similarity
	using previous positions for graph point initialization to keep clusters in same area after new optimization
	"""

	from sklearn import manifold
	
	tsne = manifold.TSNE(metric='cosine', init=data_initial)
	data_reduced_tsne = tsne.fit_transform(data)
	return data_reduced_tsne


def run_tsne_kl(data):
	"""
	compute t-distributed stochastic neighbor embedding

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties

	Returns
	-------
	object : dict-entry
		dictionary entry holding embedded data cooridnates and kl-divergence from optimization

	Providing Attributes
	-------
	embedding_ : array-like, shape (n_samples, n_components)
		Stores the embedding vectors.
	kl_divergence_ : float
		Kullback-Leibler divergence after optimization.

	Notes
	------
	using cosine as metric since data is computed to represent cosine similarity
	saving kl-divergence and data to choose embedding with lowest kl-divergence afterwards
	"""

	from sklearn import manifold
	
	tsne = manifold.TSNE(metric='cosine')
	data_reduced_tsne = tsne.fit_transform(data)
	divergence = tsne.kl_divergence_
	return {"data":data_reduced_tsne, "kl_divergence":divergence}

def run_isomap(data):
	"""
	compute isomap embedding

	Parameters
	----------
	data : ndarray
		numpy array containing cooridnates/similarities/properties

	Returns
	-------
	data_reduced_isomap : ndarray
		data mapped into n_components dimensions

	Providing Attributes
	-------
	embedding_ : array-like, shape (n_samples, n_components)
		Stores the embedding vectors.
	kernel_pca_ : object
		KernelPCA object used to implement the embedding.
	training_data_ : array-like, shape (n_samples, n_features)
		Stores the training data.
	nbrs_ : sklearn.neighbors.NearestNeighbors instance
		Stores nearest neighbors instance, including BallTree or KDtree if applicable.
	dist_matrix_ : array-like, shape (n_samples, n_samples)
		Stores the geodesic distance matrix of training data.

	Notes
	------
	n_neighbors = 30 similar to tsne perplexity
	embed into two dimensions
	"""

	from sklearn import manifold

	isomap = manifold.Isomap(n_neighbors=30, n_components=2)
	data_reduced_isomap = isomap.fit_transform(data)
	return data_reduced_isomap

def visualize_2d(data):
	"""
	visualize input data as scatterplot

	Parameters
	----------
	data : ndarray
		numpy array containing data cooridnates

	Notes
	------
	visualize data as t-SNE Scatter Plot
	"""

	from matplotlib import pyplot as plt
	from matplotlib import colors

	plt.figure()
	x = data[:,0]
	y = data[:,1]

	plt.scatter(x, y, alpha=0.5)
	plt.title("t-SNE Scatter Plot")
	#plt.savefig('myfig.svg')
	plt.show()


def visualize_2d_label(data, labels, title):
	"""
	visualize input data as scatterplot

	Parameters
	----------
	data : ndarray
		numpy array containing data cooridnates
	labels : ndarray
		array containing label information
	title : string
		plot tilte

	Notes
	------
	make use of use('Agg') to save plot as SVG image
	"""

	#from matplotlib import use
	# use('Agg')
	from matplotlib import pyplot as plt
	from matplotlib import colors

	plt.figure()
	x = data[:,0]
	y = data[:,1]

	# color_list = ['black', 'blue', 'purple', 'yellow', 'white', 'red', 'lime', 'cyan', 'orange', 'gray']
	# colors = [color_list[label] for label in labels]

	min_id = min(labels)
	max_id = max(labels)
	norm = colors.Normalize(vmin=min_id, vmax=max_id)
	cmap = plt.cm.jet
	m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
	colors = [m.to_rgba(label) for label in labels]

	plt.scatter(x, y, alpha=0.5, color=colors)
	plt.title(title)
	#plt.savefig('myfig-3.svg')
	plt.show()


def plot_pca_variance(data):
	"""
	plot cummulated explained variance ratio

	Parameters
	----------
	data : ndarray
		numpy array containing data cooridnates

	Notes
	------
	fix n_components to match input length
	"""

	from sklearn import preprocessing, decomposition
	import matplotlib.pyplot as plt
	import numpy as np

	data_scaled = preprocessing.scale(data)
	pca = decomposition.PCA(n_components=300)
	data_reduced_pca = pca.fit_transform(data_scaled)

	plt.plot(np.cumsum(pca.explained_variance_ratio_))
	plt.ylabel('Explained Variance Ratio')
	plt.xlabel('Principal Components')
	plt.show()


def find_neighbors(dataset, coordinates, n_neighbors):
	"""
	compute coordinates from n_neighbors nearest neighbors

	Parameters
	----------
	dataset : ndarray
		numpy array containing cooridnates
	coordinates : [x, y]
		array holding coordinates for neighbor search
	n_neighbors : int
		number of nearest neighbors to be computed

	Returns
	-------
	neighbors : ndarray
		array of coordinates from nearest neighbors

	Notes
	------
	don't return distances, just coordinates from nearest neighbors
	"""

	from sklearn.neighbors import NearestNeighbors

	nn_search = NearestNeighbors(n_neighbors=n_neighbors).fit(dataset)
	neighbors = nn_search.kneighbors(coordinates, return_distance=False)

	return neighbors


def save_array_txt(filename, array):
	"""
	save array as txt file

	Parameters
	----------
	array : ndarray
		numpy array containing data
	filename : string
		filename including path to store data

	Notes
	------
	used to show nearest neighbor content as txt
	add linespace after each entry
	"""

	file = open(filename, "w")
	file.write("\n\n".join(str(x) for x in array))
	file.close()


def nn_content(coordinates, n_neighbors, view):
	"""
	compute nearest neighbor content

	Parameters
	----------
	coordinates : ndarray
		numpy array containing cooridnates to search in
	n_neighbors : int
		number of neighbors to be computed
	view : ndarray
		array holding the current view path i.e. [0, 1, 0, 0]

	Returns
	-------
	neighbors_content : ndarray
		array containing the nearest neighbors content

	Notes
	------
	read forum data from specified file, need to be fixed to dynamical inputs
	"""

	# nn computation only on [x y] without labelinfo
	# get dataset from view


	# compute data_coordinates (list of [x y]) from given view
	if len(view) > 0:
		view_str = ''.join(map(str, view))
		file_path = root_directory + "/view-" + view_str + ".npy"
		data_tsne = load_array(file_path).tolist()
	else:
		data_tsne = load_array(root_directory + "/view-root.npy").tolist()

	data_coordinates = [point[:2] for point in data_tsne]

	# get IDs from current view
	# project IDs from current view on original dataset
	neighbors = find_neighbors(data_coordinates, coordinates, n_neighbors + 1)

	# compute node from view
	node = tree[len(tree) - 1]
	node_index = ""
	for decision in view:
		node_index = node[decision]
		node = tree[node_index - len(data)]
	# traverse tree and sort indicies
	data_indices = traverse_tree(tree, node, len(data))
	data_indices.sort()

	# add neighbor content to list and return result
	neighbors_content = list()
	for neighbor in neighbors[0]:
		thread_index = data_indices[neighbor]
		neighbors_content.append(file["data"][thread_index])

	return neighbors_content[1 : n_neighbors + 1]	# dont retun first item, since it is the identity of the input point


def traverse_tree(tree, node, n_samples):
	"""
	traverse tree array from given node

	Parameters
	----------
	tree : ndarray
		tree nodes represented as array
	node : ndarray [left, right]
		node containing information to its left and right child index
	n_samples : int
		size of input dataset

	Returns
	-------
	return_list : list
		list containing leaf ids

	Notes
	-----

	"""

	return_list = []

	node_stack = []
	node_stack.append(node)

	# while stack isn't empty pop item and handle its child-nodes
	while len(node_stack) != 0:
		node_current = node_stack.pop()

		# if index of left child is < n_samples it is an object from the input data otherwise it is another cluster
		if node_current[0] < n_samples:
			return_list.append(node_current[0])								# add object id to output list if it isn't a cluster
		elif node_current[0] >= n_samples:
			node_stack.append(tree[node_current[0] - n_samples])			# add left child-node to stack if it is another node

		# handle right child-node analogoues
		if node_current[1] < n_samples:
			return_list.append(node_current[1])
		elif node_current[1] >= n_samples:
			node_stack.append(tree[node_current[1] - n_samples])

	return return_list


def generate_labels(tree, node, n_samples):
	"""
	generate list of labels for given node

	Parameters
	----------
	tree : ndarray
		tree nodes represented as array
	node : ndarray [left, right]
		node containing information to its left and right child index
	n_samples : int
		size of input dataset

	Returns
	-------
	result_labels : list
		list of 0/1 at corresponding index from original dataset

	Notes
	-----
	compute list containing label information
	left child leafs 0
	right child leafs 1
	output list of size n_samples containing a 0/1 at corresponding index
	"""

	result_l = traverse_tree(tree, tree[node[0] - n_samples], n_samples)
	result_r = traverse_tree(tree, tree[node[1] - n_samples], n_samples)

	# initialize list with 2
	labels = [2] * n_samples

	# insert left and right child node labels
	for i in result_l:
		labels[i] = 0

	for i in result_r:
		labels[i] = 1

	# only keep entries smaller than 2 to match the size of the current tsne embedding
	result_labels = []
	for index in range(len(labels)):
		if labels[index] < 2:
			result_labels.append(labels[index])

	return result_labels


def generate_view(tree, node, view):
	"""
	generate the current view

	Parameters
	----------
	tree : ndarray
		tree nodes represented as array
	node : ndarray [left, right]
		node containing information to its left and right child index
	view : ndarray [0, 1, 0, 0]
		navigation path to current view

	Returns
	-------
	data_tsne : list
		list containing point information as [x y label]

	Notes
	-----
	1. compute list objects within the node
	2. generate dict mapping from index to data object
	3. generate list from key ordered dict
	(compute npy array of previous positions)
	4. compute tsne embedding
	5. zip tsne coordinates with labeldata
	"""

	import os.path
	import numpy as np

	# generate dict from index to data object
	data_indices = traverse_tree(tree, node, len(data))
	data_dict = {}
	for index in data_indices:
		data_dict[index] = data[index]

	# compute list of data objects from ordered dict
	data_sorted = []
	for key in sorted(data_dict):
		data_sorted.append(data_dict[key])

	# load previous tsne embedding and use coordinates as initial position for new embedding
	if len(view) > 0:
		view_str = ''.join(map(str, view))
		view_last = view_str[-1:]			# get the last decision to compute which points from previous data to keep for initial coordinates
		view_str = view_str[:-1]			# get the previous decisions as string to load file

		if view_str == "":
			print "view string empty, set to root"
			view_str = "root"

		file_path = root_directory + "/view-" + view_str + ".npy"
		file_exists = os.path.isfile(file_path)

		if file_exists:
			data_previous_tsne = load_array(file_path)
			# select the points for the current data_view from previous embedding
			data_previous = list()
			for point in data_previous_tsne:				# for each [x y label]
				if str(int(point[2])) == view_last:			# if label == last decision 0/1
					data_previous.append(point[:2])			# keep only the coordinates of this point
			#data_previous = [point[:2] for point in data_previous_tsne]

	# compute tsne for object within the given node
	if len(data_previous) > 0:
		data_tsne = run_tsne(data_sorted, np.array(data_previous)).tolist()
	else:
		data_tsne = run_tsne(data_sorted).tolist()

	# zip tsne coordinates and labeldata
	data_labels = generate_labels(tree, node, len(data))
	for index in range(len(data_tsne)):
		data_tsne[index].append(data_labels[index])

	return data_tsne


def get_view(view):
	"""
	compute current view

	Parameters
	----------
	view : list [0, 1, 0, 1, 0, 1]
		list containing the path/decisions (0 = left child, 1 = right child, starting from root node) to current view

	Returns
	-------
	result_view : ndarray
		array of points [x y label] for given view

	Notes
	-----
	if file exists -> load view
	else compute required coordinates and labels and save file as npy array

	conventions:
	root directory containing initial tsne commit id "data-COMMMITID"
		view-STR.npy -> view-root.npy, view-0.npy, view-001011100.npy, ...
	"""

	import os.path

	view_str = ''.join(map(str, view))
	file_path = root_directory + "/view-" + view_str + ".npy"
	file_exists = os.path.isfile(file_path)

	if view == []:
		print "load root node"
		return load_array(root_directory + "/view-root.npy")

	result_view = ""
	if file_exists:
		print "file exists"
		# read file and return data

		result_view = load_array(file_path)
	else:
		print "file doesn't exist"
		# compute and save data objects then run get_view again recursively

		# compute node from list
		node = tree[len(tree) - 1]
		node_index = ""
		for decision in view:
			node_index = node[decision]
			node = tree[node_index - len(data)]

		# compute view from given node, save it, return result through recusive function call
		view_new = generate_view(tree, node, view)
		save_array(view_new, file_path)

		result_view = get_view(view)

	return result_view


def thread_content(point, view):
	""""
	retrieve thread content from given point

	Parameters
	----------
	point : list [x, y, label]
		point information
	view : list [0, 1, 0, 1, 0, 1]
		list containing the path/decisions (0 = left child, 1 = right child, starting from root node) to current view

	Returns
	-------
	thread_content : json-object
		content information of given point

	Notes
	-----

	"""

	# point [x y label]
	# compute index in tsne embedding
	# - load tsne embedding from npy array file
	
	if len(view) > 0:
		view_str = ''.join(map(str, view))
		file_path = root_directory + "/view-" + view_str + ".npy"
		data_tsne = load_array(file_path).tolist()
	else:
		data_tsne = load_array(root_directory + "/view-root.npy").tolist()
	point_index = data_tsne.index(point)

	# traverse tree from given view
	# - compute node from view
	node = tree[len(tree) - 1]
	node_index = ""
	for decision in view:
		node_index = node[decision]
		node = tree[node_index - len(data)]
	# - traverse tree and sort indicies
	data_indices = traverse_tree(tree, node, len(data))
	data_indices.sort()

	# select item from tree with index computet from tsne
	thread_index = data_indices[point_index]

	# return original dataset json[item]
	thread_content = file["data"][thread_index]

	return thread_content


# pre-load required npy files
file = read_json(threads_json)
data = load_array('files/threads-pca-30-17b145e0.npy')
data_tsne_complete = load_array('files/tsne-9be223ea.npy')
# data = read_csv(threads) # work on pca reduced data only