# Interactive Visualizer

The Interactive Visualizer is a tool designed to help you explore and grasp big data collections. Visualize the structure of your data and browse them interactively by navigating through their hierarchy and using a visualization technique preserving neighborhood structure.

## Requirements
A requirements.txt file is included to provide the necessary python package dependencies. Use `pip install -r requirements.txt` to install all packages via pip.

## How To Start
1. Open init.py, modify the config (see section Config below) and uncomment `# init()`
2. Run `python init.py` via terminal to compute initial two dimensional embedding and hierarchy
3. Re-comment the line from step 1.
4. Run `python webservice.py` via terminal and access the interactive visualizer web application at `http://localhost:5000/visualizer/`

Config:
    - bool use_pca_data : true to work on 100 principal components instead of full dimensionality on original dataset
    - int n_tsne_runs : number of iterations to choose optimal kl-divergence for initial embedding
    - string root_directory : path to save all embeddings and hierarchy
    - string threads : path to original dataset (as csv)
    - string threads_json : path to original dataset contents (as json)
    - nd_array tree : hierarchy computed from clustering (stored in root_directory + 'hierarchy.npy' after running init.py)

Notes: To avoid concurrency on this port add parameter port=8080 (or any other desired free port on the host) to app.run in webservice.py (last line)


## ToDo-List
- [ ] terminate zoom/navigation for leaf nodes and restrict the use of navigation buttons
- [ ] use url parameter to hold current view (avoids conflicts in web visualization and backend data points)
- [ ] implement option to use the best tsne embedding out of ten (refactor commented code into tsne computation)

## Further Information
This tool has been developed during the work for my bachelors thesis at the chair of artificial intelligence at the department of computer science at the Technical University Dortmund. It combines the use of the t-Distributed Stochastic Neighbor Embedding and a hierarchical clustering to tackle the amount of data and dimensionality in datasets. Since the use in a real world problem in analyzing threads to explore the structure of a forum I hope this might be helpful for someone else.